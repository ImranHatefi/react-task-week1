import React from 'react';

const ListTitle = () => React.createElement('h1', {
    style: {
     color: '#999', fontSize: '19px'
    }
}, 'Solar system planets')

export default ListTitle;