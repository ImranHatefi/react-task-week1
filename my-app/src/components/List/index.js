import './List.css'

function List() {
  const planets = [
    "Mercury",
    "Venus",
    "Earth",
    "Mars",
    "Jupiter",
    "Saturn",
    "Uranus",
    "Neptune",
  ];
  return (
    <ul className="planets-list">
      {planets.map((planet) => (
        <li key={planet}>{planet}</li>
      ))}
    </ul>
  );
}

export default List;
