import ListTitle from "./listTitle"
import List from "./List"

export function Main(){
    return <main style={{
        margin: 20
    }}>
        <ListTitle/>
        <List/>
     </main>
}